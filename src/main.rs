//    LLamabot, an irc interface to llambdac
//    Copyright (C) 2017 Waylon Cude
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

extern crate irc;
extern crate regex;
use regex::Regex;
use std::io::Write;
use std::str;
use std::process::{Command, Stdio};
use std::thread;
use std::sync::Arc;
use regex::Captures;
use irc::client::prelude::*;

fn main() {
    let server = IrcClient::new("config.json").unwrap();
    println!("Indentifying");
    server.identify().unwrap();
    println!("Indentified");
    let matches_name = Regex::new(r"llamabot: ?(.*)$").unwrap();
    //let php = Regex::new(r"<\?llama(.*?)\?>").unwrap();
    let server = Arc::new(server);
    server.for_each_incoming(|m| {
        if let irc::proto::command::Command::PRIVMSG(chan,message) = m.command {
            if let Some(cap) = matches_name.captures(&message) {
                match &cap[1] {
                    "source" => {
                        server.send_privmsg(chan.as_str(),"Source available at https://gitlab.com/waylon531/llamabot").unwrap();
                    },
                    "help" => {
                        server.send_privmsg(chan.as_str(),"IRC frontend for llambdac. Enclose commands in PHP-like tags.").unwrap();
                        server.send_privmsg(chan.as_str(),r"Example: `two times four is <?llama let double = \x.(* x 2);  ( double 4 )?> `").unwrap();
                        server.send_privmsg(chan.as_str(),r"two times four is 8").unwrap();
                        server.send_privmsg(chan.as_str(),r"Example: `llamabot: let double = \x.(* x 2);  ( double 4 )`").unwrap();
                        server.send_privmsg(chan.as_str(),r"8").unwrap();
                        server.send_privmsg(chan.as_str(),r"Documentation at: http://web.cecs.pdx.edu/~cudew/LLambai_Docs.pdf").unwrap();
                    },
                    _ => {
                        let cloned_server = server.clone();
                        let cap_clone = cap[1].to_owned();
                        thread::spawn( move || {
                            //let php = Regex::new(r"<\?llama(.*?)\?>").unwrap();
                            //if php.is_match(&cap_clone) {
                            let final_str: String;
                            //{ //php.replace_all(&cap_clone ,|capture: &Captures| {
                            let mut child = Command::new("/home/cudew/ubuntu/bin/llambdai")
                                .stdout(Stdio::piped())
                                .stderr(Stdio::piped())
                                .stdin(Stdio::piped())
                                .spawn()
                                .expect("failed to execute child");

                            &mut child.stdin.take().unwrap()
                                .write_all(cap_clone.as_bytes()).expect("Failed to write to child's stdin");

                            //let mut message = [0u8; 512];
                            //child.wait_with_output().expect("Bad output").std.read(&mut message).expect("Failed to read child message");
                            let output = 
                                &child.wait_with_output().expect("Bad output");

                            cloned_server.send_privmsg(chan.as_str(),str::from_utf8(
                                    &output.stderr
                                    ).unwrap()).expect("Failed to send message");
                            final_str = str::from_utf8(&output.stdout).unwrap().trim_right().to_owned();
                            cloned_server.send_privmsg(chan.as_str(),final_str.as_str()).expect("Failed to send message");
                        });
                    }
                }
                // Early return to free up message for moving into thread
                return;
            }; 
            // The following code is basically an else branch,
            // but it has to be in a different scope to release the
            // borrow on message
            let cloned_server = server.clone();
            thread::spawn(move || replace_php(cloned_server,message,&chan));
        };
    }).expect("Failed to get messages");
}

fn replace_php(cloned_server: Arc<IrcClient>, cap_clone: String, chan: &str) {
    let php = Regex::new(r"<\?llama(.*?)\?>").unwrap();
    if php.is_match(&cap_clone) {
        let final_str = php.replace_all(&cap_clone ,|capture: &Captures| {
            let mut child = Command::new("/home/cudew/ubuntu/bin/llambdai")
                .stdout(Stdio::piped())
                .stderr(Stdio::piped())
                .stdin(Stdio::piped())
                .spawn()
                .expect("failed to execute child");

            &mut child.stdin.take().unwrap()
                .write_all(capture[1].as_bytes()).expect("Failed to write to child's stdin");

            //let mut message = [0u8; 512];
            //child.wait_with_output().expect("Bad output").std.read(&mut message).expect("Failed to read child message");
            let output = 
                &child.wait_with_output().expect("Bad output");

            cloned_server.send_privmsg(chan,str::from_utf8(
                    &output.stderr
                    ).unwrap()).expect("Failed to send message");
            str::from_utf8(&output.stdout).unwrap().trim_right().to_owned()
        });
        cloned_server.send_privmsg(chan,final_str.into_owned().as_str()).expect("Failed to send message");
    }
}
